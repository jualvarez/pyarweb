FROM python:3.4
ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH /code:$PYTHONPATH

# runtime dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
    python3-dev \
    libxml2-dev \
    libxslt1-dev \
    zlib1g-dev \
    libffi-dev \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir /code
WORKDIR /code
# update pip (to use wheels)
RUN wget -O - https://bootstrap.pypa.io/get-pip.py | python3

# RUN mkdir ~/.pip
# RUN echo "[global]  \nindex-url = http://10.42.0.82:3141/root/pypi/+simple/ \ntrusted-host = 10.42.0.82" > ~/.pip/pip.conf
# RUN cat ~/.pip/pip.conf
COPY dev_requirements.txt /code
COPY requirements.txt /code
RUN pip install -r dev_requirements.txt
COPY . /code/

RUN export LANG=en_US.UTF-8

RUN mkdir /var/log/pyarweb/
RUN touch /var/log/pyarweb/pyarweb.log
RUN touch /var/log/pyarweb/pyarweb_access.log

RUN python3 /code/manage.py collectstatic --noinput

CMD gunicorn pyarweb.wsgi:application \
    --pid /tmp/pyar_web.pid \
    --workers 6 \
    --bind 0.0.0.0:5000 \
    --log-level=debug \
    --access-logfile=/var/log/pyarweb/pyarweb_access.log
EXPOSE 5000
